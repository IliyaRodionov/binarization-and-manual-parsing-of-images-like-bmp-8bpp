#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <rpc.h>
#include <wingdi.h>
#include "FreeImage.h"

/*
 * Данный метод возвращает указатель на массив яркостей
 * */
double ** getArrayBrightnessPixel(char * in_image_path, double **img_brightness, int * size_image_height, int * size_image_width);
/*
 * Метод niblack возвращает указатель на массив данных бинаризации изображения
 * на вход подается:
 * - указатель на массив яркостей
 * - указатель на массив бинаризации
 * - размеры изображения
 * - размер окна для работы алгоритма Ниблэка
 * - значение смещения "к" для работы алгоритма Ниблэка
 * */
char ** niblack(double **img_brightness, char **img_binary, int size_image_height, int size_image_width, int size_window,
             double k);
/*
 * Метод threshold возвращает 1 или 0 в зависимоти результата работы алгоритма Ниблэка. Сравнивается реальная яркость пикселя с пороговым
 * значением яркости, которое расчитывается по алгоритму Ниблэка
 * На вход принимаются:
 * - i, j значения индекса сдвига окна. Иллюстрация алгоритма движения окна представлена в документе "Сопроводительное пиьсмо".
 * когда значения индекса пикселя превыщаю середину окна, они изменяются и перемещают окно.
 * - w - размер окна
 * - к - коэффициент(смещение)
 * - n размер изображения ( высота - количество строк)
 * - m размер изображения (длина - количество столбцов)
 * */
char threshold(double **img_brightness, int number_of_columns_in_an_array_brightness,int i, int j, int w, double k, int n, int m);
/*
 * Записывает обработанное изображение в новый файл
 * На вход принимается:
 * - путь до входного изображения
 * - путь до выодного изображения
 * - массив бинаризации
 * */
void write_a_new_binarization_image(char * in_image_path, char * out_image_path, char ** img_binary);
/*
 * Расчет среднего значения
 * */
double mean_local_area(int i, int j, int w, double **img_brightness, int number_of_columns_in_an_array_brightness);
/*
 * Расчет стандартного отклонения
 * */
double deviation_local_area(int i, int j, int w, double x_mean, double **img_brightness, int number_of_columns_in_an_array_brightness);
/*
 * Освобождение памяти
 * */
void freeing_up_memory(void ** A, size_t n);
/*
 * Функции вывода данных на экран
 * */
void dynamic_array_print_Char(char ** array, size_t n, size_t m);
void dynamic_array_print_Double(double ** array, size_t n, size_t m);

typedef struct __attribute__((__packed__)) {
    unsigned char  p;
} IMAGE;

int main() {

    char *name_input_image_file = "C:\\Users\\ir_000\\CLionProjects\\niblack\\einstein.jpg";
    char *name_output_image_file = "C:\\Users\\ir_000\\CLionProjects\\niblack\\binariza_einstein.jpg";

    int size_image_height = 0;
    int size_image_width = 0;

    //Указатель на массив яркостей
    double **img_brightness = getArrayBrightnessPixel(name_input_image_file, img_brightness, &size_image_height, &size_image_width);
   // dynamic_array_print_Double(img_brightness, size_image_height, size_image_width);

    //Указатель на массив бинаризации
    char **img_binary = niblack(img_brightness, img_binary, size_image_height, size_image_width, 15, 0.2);
    dynamic_array_print_Char(img_binary, size_image_height,size_image_width);

    //Записываем новый файл
    write_a_new_binarization_image(name_input_image_file, name_output_image_file, img_binary);

    freeing_up_memory(img_brightness, size_image_height);
    freeing_up_memory(img_binary, size_image_width);

    return 0;
    }

double ** getArrayBrightnessPixel(char * in_image_path, double **img_brightness, int * size_image_height, int * size_image_width){

    FILE *fp;
    int offset_palitra = 0;     //смещение по файлу до начала данных цветовой палитры
    int len_array_palette = 256;   //стандартный набор цветов палитры в файлах bmp
    int i = 0, j = 0, n = 0, index_palette = 0;
    double a = 0.2126, b = 0.7152, c = 0.0722;  //коэффициенты для рассчета яркости пикселя RGB

    // Объявляем структуры
    BITMAPFILEHEADER bitmapfileheader;
    BITMAPINFOHEADER bitmapinfoheader;
    RGBQUAD RGB_pixel[len_array_palette];

    if((fp=fopen(in_image_path, "rb+"))==NULL) {
        printf ("Cannot open file.\n");
    }

    fread(&bitmapfileheader,sizeof(bitmapfileheader),1,fp);               //Записываем файловый заголовок в структуру BITMAPFILEHEADER
    fread(&bitmapinfoheader,sizeof(bitmapinfoheader),1,fp);               //Записываем заголовок изображения в структуру BITMAPINFOHEADER

    //записываем размеры изображения, чтобы ими можно было оперировать в main
    *size_image_height = bitmapinfoheader.biHeight;
    *size_image_width = bitmapinfoheader.biWidth;

    //рассчитываем смещение до цветовой палитры и перемещаем указатель на начало данных цветовой палитры (относительно начала файла)
    offset_palitra = sizeof(bitmapfileheader) + sizeof(bitmapinfoheader);
    fseek(fp, offset_palitra, SEEK_SET);

    //заполняем массив структур RGB палитры из файла
    for(n = 0; n < len_array_palette; n++){
        fread(&RGB_pixel[n], sizeof(unsigned char), sizeof(RGBQUAD), fp);
    }

    //объявляем структуру для пиксельных данных и указываем размерность по известным данным изображения
    IMAGE img[bitmapinfoheader.biHeight][bitmapinfoheader.biWidth];

    //выделяем память для массива яркостей и сдвигаем указатель на начало пиксельных данных
    //img_brightness = (double*)malloc(bitmapinfoheader.biHeight*bitmapinfoheader.biWidth* sizeof(double));
    img_brightness = (double**)malloc(bitmapinfoheader.biHeight* sizeof(double*));
    for(int k = 0; k  < bitmapinfoheader.biHeight; k++){
        img_brightness[k] = (double*)malloc(bitmapinfoheader.biWidth* sizeof(double));
    }
    fseek(fp, bitmapfileheader.bfOffBits, SEEK_SET);

    //так как в файле bmp пиксельные данные перевернуты, т.е. начало изображения находится внизу, начинаем заполнение массива снизу
    for (i = bitmapinfoheader.biHeight-1; i >= 0; i--) {
        for (j = 0; j < bitmapinfoheader.biWidth; j++) {

            fread(&img[i][j], sizeof(unsigned char), sizeof(IMAGE), fp);
            //в случае 8-ми битного описания пикселя в файле bmp, в пиксельных данных хранится индекс цветовой палитры
            //по данному индексу, будет производиться обращение в массив структур палитры
            index_palette = img[i][j].p;
            //рассчитываем значение яркости
            //и заполнаем клон-массив яркостей пикселей
            //*(img_brightness + i*bitmapinfoheader.biWidth + j) = (a * RGB_pixel[index_palette].rgbtRed) + (b * RGB_pixel[index_palette].rgbtGreen) + (c * RGB_pixel[index_palette].rgbtBlue);
            img_brightness[i][j] = (a * RGB_pixel[index_palette].rgbRed) + (b * RGB_pixel[index_palette].rgbGreen) + (c * RGB_pixel[index_palette].rgbBlue);

        }

    }

    printf("getArrayBrightnessPixel - done\n");
    fclose(fp);
    return img_brightness;
}

char ** niblack(double **img_brightness, char **img_binary, int size_image_height, int size_image_width, int size_window,
             double k){

    int i = 0, j = 0;   //счетчики сдвига окна
    int n, m;   //индексы пробега по пиксельным данным

    //выделяем память для массива бинаризации
    //img_binary = (char *)malloc(size_image_height*size_image_width* sizeof(char));
    img_binary = (char **)malloc(size_image_height * sizeof(char *));
    for(int g = 0; g  < size_image_height; g++){
        img_binary[g] = (char*)malloc(size_image_width * sizeof(char));
    }

    int start_move_window = size_window/2 + 1;  //считаем середину окна (только для нечетных значения размера окна)
    int start_move_window_y = size_image_height - 1 - start_move_window;
    int stop_move_window_y = size_image_height - 1 - start_move_window_y;
    int start_move_window_x = start_move_window; //остановка сдвига окна по горизонтали
    int stop_move_window_x = size_image_width - start_move_window;  //остановка сдвига окна по вертикали

    for(n = size_image_height -1; n >= 0; n--){

        if(n <= size_image_height -1 && n > start_move_window_y){
            i = size_image_height - 1;
        } else if (n <= start_move_window_y && n >= stop_move_window_y){
            i-=1;
        }

        for(m = 0; m < size_image_width; m++){

            if(m >= 0 && m < start_move_window_x){
                j = 0;
            } else if (m >= start_move_window_x && m <= stop_move_window_x){
                j+=1;
            }

            //*(img_binary + n*size_image_width + m) = threshold(img_brightness, size_image_width, i, j, size_window, k, n, m);
            img_binary[n][m] = threshold(img_brightness, size_image_width, i, j, size_window, k, n, m);

        }
    }

    printf("niblack - done\n");
    return img_binary;
}

char threshold(double **img_brightness, int number_of_columns_in_an_array_brightness, int i, int j, int w, double k, int n, int m) {

    double x_mean = 0;
    double d_deviation = 0;
    double value_threshold = 0;     //пороговое значение по алгоритму Niblack
    double I = img_brightness[n][m];    //значение пикселя, относительно которого считается пороговое значение и которое сравнивается с полученным пороговым значением


    x_mean = mean_local_area(i, j, w, img_brightness, number_of_columns_in_an_array_brightness);
    d_deviation = deviation_local_area(i, j, w, x_mean, img_brightness, number_of_columns_in_an_array_brightness);

    value_threshold = x_mean + k * d_deviation;

    //бинаризация с нижним порогом
    if (I >= value_threshold) {

        return 0;

    } else if (I < value_threshold) {

        return 1;

    }

    /*
    //бинаризация с верхним порогом
    if(I <= value_threshold){

        return 0;

    } else if(I > value_threshold){

        return 1;

    }
    */
}

void write_a_new_binarization_image(char * in_image_path, char * out_image_path, char ** img_binary){

    FILE *fp_in;
    FILE *fp_out;
    int offset_palitra = 0;     //смещение по файлу до начала данных цветовой палитры
    int i = 0, j = 0, n = 0;
    unsigned char index_palette = 0;
    int len_array_palette = 256;
    size_t padding = 0;

    // Объявляем структуры
    BITMAPFILEHEADER bitmapfileheader;
    BITMAPINFOHEADER bitmapinfoheader;
    RGBQUAD RGB_pixel[len_array_palette];

    if((fp_in=fopen(in_image_path, "rb+"))==NULL) {
        printf ("Cannot open file.\n");
    }
    if((fp_out=fopen(out_image_path, "w+b"))==NULL) {
        printf ("Cannot open file.\n");
    }

    fread(&bitmapfileheader,sizeof(bitmapfileheader),1,fp_in);               //Читаем файловый заголовок в структуру BITMAPFILEHEADER
    fwrite(&bitmapfileheader, sizeof(bitmapfileheader), 1, fp_out);          //Записываем в новый файл
    fread(&bitmapinfoheader,sizeof(bitmapinfoheader),1,fp_in);               //Читаем заголовок изображения в структуру BITMAPINFOHEADER
    fwrite(&bitmapinfoheader, sizeof(bitmapinfoheader), 1, fp_out);         //Записываем в новый файл
    //расчитываем смещение
    if ((bitmapinfoheader.biWidth * 3) % 4)
        padding = 4 - (bitmapinfoheader.biWidth * 3) % 4;

    //рассчитываем смещение до цветовой палитры и перемещаем указатель на начало данных цветовой палитры (относительно начала файла)
    offset_palitra = sizeof(bitmapfileheader) + sizeof(bitmapinfoheader);
    fseek(fp_in, offset_palitra, SEEK_SET);

    //заполняем массив структур RGB палитры из файла и тут же записываем в новый файл
    for(n = 0; n < len_array_palette; n++){
        fread(&RGB_pixel[n], sizeof(unsigned char), sizeof(RGBQUAD), fp_in);
        fwrite(&RGB_pixel[n], sizeof(unsigned char), sizeof(RGBQUAD), fp_out);
    }
    /*  //записываем массив структур RGB палитры из файла in в файл out
      for(n = 0; n < len_array_palette; n++){
          fwrite(&RGB_pixel[n], sizeof(unsigned char), sizeof(RGBQUAD), fp_out);
      }
  */
    /*
     * Так как в файле bmp при 8-битном описании пикселя в пиксельных данных хранится значение индекса палитры,
     * создадим новый индекс палитры:
     * -если в ячейке массива бинаризации хранится 0 - запишем значение индекса палитры = 0, так как
     * в нулевом индексе хранится черный цвет {0,0,0,0};
     * -если в ячейке массива бинаризации хранится 1 - запишем значение индекса палитры = 255, так как
     * в 255 индексе хранится белый цвет {255,255,255,0};
     * */

    //так как в файле bmp пиксельные данные перевернуты, т.е. начало изображения находится внизу, начинаем заполнение массива снизу
    for (i = bitmapinfoheader.biHeight-1; i >= 0; i--) {
        for (j = 0; j < bitmapinfoheader.biWidth; j++) {

            if(img_binary[i][j] == 0){

                index_palette = 0;
                fwrite(&index_palette, sizeof(unsigned char), sizeof(index_palette), fp_out);

            } else if(img_binary[i][j] == 1){

                index_palette = 255;
                fwrite(&index_palette, sizeof(unsigned char), sizeof(index_palette), fp_out);

            } else {
                printf("Массив бинаризации содержит значения отличные от 0 или 1! Введите корректные даные!");
                return;
            }

        }
        if(padding != 0) {
            fwrite(&index_palette, padding, 1, fp_out);
        }
    }

    fclose(fp_in);
    fclose(fp_out);

}

double mean_local_area(int i, int j, int w, double **img_brightness, int number_of_columns_in_an_array_brightness) {

    int line_y = i;
    int line_x = j;
    double sum_for_mean = 0;
    double x_mean = 0;
    int number_of_members = w*w;

    for (line_y = i; line_y > i - w; line_y--) {

        for (line_x = j; line_x < w + j; line_x++) {

            sum_for_mean += img_brightness[line_y][line_x];

        }

    }

    x_mean = sum_for_mean/number_of_members;

    return x_mean;
}
double deviation_local_area(int i, int j, int w, double x_mean, double **img_brightness, int number_of_columns_in_an_array_brightness){

    int line_y = i;
    int line_x = j;

    double sum_for_deviation = 0;
    double x_deviation = 0;
    int number_of_members = w*w;

    for (line_y = i; line_y > i - w; line_y--) {

        for (line_x = j; line_x < w + j; line_x++) {

            sum_for_deviation += pow((img_brightness[line_y][line_x] - x_mean), 2);

        }

    }

    x_deviation = sqrt(sum_for_deviation/number_of_members);

    return x_deviation;

}

void freeing_up_memory(void ** A, size_t n){

    for (int i = 0; i < n; i++) {
        free(A[i]);
    }
    free(A);
}

void dynamic_array_print_Char(char** array, size_t n, size_t m) {

    for (int i = 511; i >= 0; i--) {
        for (int j = 0; j < 10; j++) {

            printf("[%d][%d] %d \n", i, j, array[i][j]);
        }
        //printf("\n");
        break;
    }
}

void dynamic_array_print_Double(double ** array, size_t n, size_t m){

    for (int i = 511; i >= 0; i--) {
        for (int j = 0; j < m; j++) {

            printf("[%d][%d] %lf \n", i, j, array[i][j]);
        }
        //printf("\n");
        break;
    }
}